$(document).ready(function() {
	$("#lightSlider").lightSlider({
		item:1,
		gallery: true,
		thumbItem:3,
		thumbMargin: 3,
		// verticalHeight: 300,
		// vertical: true,
		onSliderLoad: function(el) {
			el.lightGallery();
		}
	});
});