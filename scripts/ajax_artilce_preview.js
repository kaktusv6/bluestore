function modal_preview(url) {
    var modal = document.getElementById("myModal");
    var close_modal = document.getElementsByClassName("close_modal")[0];

    modal.style.display = "block";

    $.ajax(url).done(function(html) {
         $('#modal_preview_content').html($(html));
    });

    close_modal.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }

}