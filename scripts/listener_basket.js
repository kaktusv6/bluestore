function add_in_sum(price) {
    all_sum += price;
}

function btn_minus(price, id) {
    var countItem = parseInt($('#count_' + id).val());
    if (countItem === 1) {
        return;
    }

    all_sum -= price;
    document.getElementById("total_sum").textContent = all_sum + " руб";
    ajaxChangeCount('minus', id, idBasket);
    return false;
}

function btn_plus(price, id) {
    all_sum += price;
    document.getElementById("total_sum").textContent = all_sum + " руб";
    ajaxChangeCount('plus', id, idBasket);
    return false;
}

function sum_item(price, input_form, id_item) {
    sum = price * input_form.value;
    document.getElementById(id_item).textContent = sum + " руб";
}

function ajaxChangeCount(action, idItem, idBasket) {
    $.ajax({
        url: '/catalog/change_basket_item/',
        type: 'POST',
        data: { idItem: idItem, idBasket: idBasket, action: action }
    });
}

function deleteItem(idItem) {
    $.ajax({
        url: '/catalog/delete_item_basket/',
        type: 'POST',
        data: {idItem: idItem, idBasket: idBasket}
    });
    location.reload();
    return false;
}
