function like(id) {
    var Data = {id_item:id};
    $.ajax({
        url : "/catalog/add_favorite/",
        type: "POST",
        data: Data
    }).done(function () {
        alert("Товар добавлен в избранное");
        // location.reload();
    });

}

function dislike(id) {
    var Data = {id_item:id};
    $.ajax({
        url : "/catalog/remove_favorite/",
        type: "POST",
        data: Data
    }).done(function () {
        alert("Товар удален из избранное");
        // location.reload();
    });

}

function add_basket(id) {
    var Data = {id_item:id};
    $.ajax({
        url : "/catalog/add_basket/",
        type: "POST",
        data: Data
    }).done(alert("Товар добавлен в корзину"));
}