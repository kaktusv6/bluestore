<?php

class Model {
	public $pdo;

	function  __construct() {
        try {
            $this->pdo = new PDO("mysql:server = localhost; Database = bluestore", "root", "");
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            print("Error connecting to SQL Server. <br>");
            print($e->getMessage());
            die();
        }
	}

	static function factory_model($nameModel) {
		$classModel = "Model_$nameModel";
		$modelFile = strtolower($classModel).".php";
		$pathModel = "application/models/".$modelFile;
		if(file_exists($pathModel)) {
			include "application/models/".$modelFile;
			return new $classModel();
		}
		else {
			return new Model();
		}
	}

    function getCategories() {
        $query = "CALL bluestore.getCategories();";
        $res = $this->pdo->query($query);
        $data = array();
        while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    function getCountItems($count) {
        $query = "select * from bluestore.items limit $count";
        $items = $this->getDataQuery($query);
        $lengthItems = count($items);

        $arIntegers = array();
        $res = array();
        for ($i = 0; $i < $count; $i++) {
            if ($i > $lengthItems) {
                break;
            }

            $rand = rand(0, $lengthItems - 1);
            while (in_array($rand, $arIntegers)) {
                $rand = rand(0, $lengthItems - 1);
            }
            $arIntegers[] = $rand;
            $res[] = $items[$rand];
        }
        return $res;
    }

    protected function getDataQuery($query = '') {
	    if (empty($query))
	        return array();

	    try {
            $res = $this->pdo->query($query);
            $data = array();
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
                $data[] = $row;
            }
            return $data;
        }
        catch (PDOException $e) {
	        echo '<pre>';
	        print_r($e->getMessage());
            echo '</pre>';
            return array();
        }
    }

    function getUsers() {
        $query = "select * from bluestore.users;";
        $data = $this->getDataQuery($query);

        return $data;
    }

    function getUser($login) {
        if (empty($login)) return array();

        $query = "select * from bluestore.users where users.login = '$login';";
        if (is_int($login)) {
            $query = "select * from bluestore.users where users.id = $login;";
        }

        $res = $this->pdo->query($query);
        $row = $res->fetch(PDO::FETCH_ASSOC);

        if (empty($row)) return array();

        return $row;
    }

    function getUserBuId($id) {
	    $query = "call bluestore.getUserById($id)";
	    return $this->getDataQuery($query);
    }

    function addImage($dataFile) {
	    $result = array();

        $target_dir = "uploads/";
//        echo basename($dataFile['name']) . '<br>';
        $target_file = $target_dir . basename($dataFile["name"]);
        $isUpload = true;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image

        if (empty($dataFile["tmp_name"])) {
            $isUpload = false;
            $result['message'] = "Tmp name is empty.";
            $result['isUpload'] = $isUpload;
            return $result;
        }

        $check = getimagesize($dataFile["tmp_name"]);
        if($check !== false) {
//            echo "File is an image - " . $check["mime"] . ".";
            $isUpload = true;
        } else {
            $isUpload = false;
            $result['message'] = "File is not an image.";
            $result['isUpload'] = $isUpload;
            return $result;
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            $isUpload = false;
            $result['message'] = "Sorry, file already exists.";
            $result['isUpload'] = $isUpload;
            return $result;
        }
        // Check file size
        if ($dataFile["size"] > MAX_LARGE_IMAGE) {
            $isUpload = false;
            $result['message'] = "Sorry, your file is too large.";
            $result['isUpload'] = $isUpload;
            return $result;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" &&
            $imageFileType != "png" &&
            $imageFileType != "jpeg" &&
            $imageFileType != "gif" ) {
            $isUpload = false;
            $result['message'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $result['isUpload'] = $isUpload;
            return $result;
        }

        // Check if $uploadOk is set to 0 by an error
        if (!$isUpload) {
            $result['message'] = "Sorry, your file was not uploaded.";
            $result['isUpload'] = $isUpload;
            return $result;
        // if everything is ok, try to upload file
        }

        if (move_uploaded_file($dataFile["tmp_name"], $target_file)) {
            $result['message'] = "The file ". basename( $dataFile["name"]). " has been uploaded.";
            $result['isUpload'] = $isUpload;
            $result['path_image'] = '/' + $target_file;
            return $result;
        }

        $isUpload = false;
        $result['message'] = "Sorry, there was an error uploading your file.";
        $result['isUpload'] = $isUpload;
        return $result;
    }

    function getItem($id) {
	    $id = intval($id);
        $query = "call bluestore.getItem($id)";
        $item = $this->getDataQuery($query);
        return $item[0];
    }

    function getFavoritsUser($idUser) {
        $query = "select id_item from bluestore.favorits where id_user = $idUser";
        $resQuery = $this->getDataQuery($query);
        $res = array();
        foreach($resQuery as $key => $id) {
            $item = $this->getItem($id['id_item']);
            $res[] = $item;
        }
        return $res;
    }

    function __destruct() {

    }
}