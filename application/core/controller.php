<?php
class Controller {
	
	public $model;
	public $view;

	public function __construct($controllerName) {
		$this->view = new View($this);
		$this->model = Model::factory_model($controllerName);
        $this->updateSession();
        $this->updateCookie();
	}
	
	function action_index() {
		$this->view->generate();
	}
	
	function get_name_controller() {
		$className = get_class($this);
		$className = explode('_', $className);
		if (isset($className[1])) {
			return strtolower($className[1]);
		}
		
		return 'main';
	}

    function isAuthUser() {
        return (isset($_COOKIE['login']) && !empty($_COOKIE['login'])) ||
            (isset($_SESSION['login']) && !empty($_SESSION['login']));
    }

    function isAdmin() {
	    return $this->isAuthUser() && (
                (isset($_COOKIE['isAdmin']) && $_COOKIE['isAdmin']) ||
                (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'])
            );
    }

    function locationTo($controller = '', $action = '') {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        if (empty($controller)) {
            header('Location: ' . $host);
            return;
        }
        if (empty($action)) {
            header('Location: ' . $host . $controller . '/');
            return;
        }

        header('Location: ' . $host . $controller . '/' . $action);
    }

    function logout() {
        session_unset();
        setcookie('login', null, time() - 1, '/');
        setcookie('isAdmin', null, time() - 1, '/');
    }

    function getCateries() {
        return $this->model->getCategories();
    }

    function updateSession() {
	    if (isset($_SESSION['login']) && !empty($_SESSION['login'])) {
	        $user = $this->model->getUser($_SESSION['login']);
            $_SESSION['login'] = $user['login'];
            $_SESSION['isAdmin'] = $user['status'];
        }
    }

    function updateCookie() {
        if (isset($_COOKIE['login']) && !empty($_COOKIE['login'])) {
            $user = $this->model->getUser($_COOKIE['login']);
            setcookie('login', $user['login'], time() + TIME_COOKIE, '/');
            setcookie('isAdmin', $user['status'], time() + TIME_COOKIE, '/');
        }
    }
}
