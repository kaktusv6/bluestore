<?php
class Model_User extends Model {
    function registerUser($data) {
        $login = $data['email'];
        $pas = md5($data['password']);
        $name = $data['name'];
        $query = "insert into bluestore.users (login, name, password, status) values ('$login', '$name', '$pas', 0);";
        $resultQuery = $this->pdo->query($query);
    }

    function changeDataUser($login, $data) {
        if (!$data || !$login) {
            return false;
        }

        $name = $data['name'];
        $secondName = $data['second_name'];
        $phone = $data['phone'];
        $country = $data['country'];
        $city = $data['city'];
        $address = $data['address'];

        if (!empty($data['password_old']) && !empty($data['password_new'])) {
            $newPas = md5($data['password_new']);
            $oldPas = md5($data['password_old']);
            $query = "UPDATE bluestore.users SET password = '$newPas' WHERE login = '$login' AND password = '$oldPas';";
            $this->pdo->query($query);
        }

        $query = "UPDATE bluestore.users
                  SET
                    name = '$name',
                    second_name = '$secondName',
                    tel = '$phone',
                    country = '$country',
                    city = '$city',
                    address = '$address'
                  WHERE
                    login = '$login'
                  ;";

        $this->pdo->query($query);

        return true;
    }

    function getItemsBasket($idUser) {
        $query = "select bluestore.items.id, bluestore.items.name, bluestore.items.price, bluestore.link_basket_iems.count, bluestore.items.count as maxCount from bluestore.items
                  inner join bluestore.link_basket_iems on bluestore.items.id = bluestore.link_basket_iems.id_item
                  inner join bluestore.baskets on bluestore.link_basket_iems.id_basket = bluestore.baskets.id
                  where bluestore.baskets.id_user = $idUser AND bluestore.baskets.is_open = 1";
        return $this->getDataQuery($query);
    }

    function checkoutOrder($idUser, $idBasket) {
        $query = "update bluestore.baskets set is_open = 0 where id = $idBasket and id_user = $idUser;";
        $this->pdo->query($query);
        $user = $this->getUser($idUser);
        $res = array();
        $res['isSuccess'] = true;
        foreach($user as $name => $field) {
            if (empty($field)) {
                $newVal = $_POST[$name];
                $query = "updste bluestore.users set $name = $newVal where id = $idUser";
                $this->pdo->query($query);
            }
        }

        foreach ($_POST as $name => $val) {
            if (empty($val)) {
                $res['isSuccess'] = false;
                $res['message'] = 'Все поля должны быть заполнены.';
                return $res;
            }
        }

        $curDate = date("Y-m-d");
        $query = "insert into bluestore.orders (id_user, id_basket, date, status) values ($idUser, $idBasket, '$curDate', 'оформлен');";
        $this->pdo->query($query);
        return $res;
    }
}