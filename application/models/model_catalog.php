<?php
class Model_Catalog extends Model {
    function getItems($id) {
        $query = "call bluestore.getItemsCategory($id);";
        return $this->getDataQuery($query);
    }

    function getComments($id) {
        $query = "select * from bluestore.comments where id_item = $id";
        return $this->getDataQuery($query);
    }

    function addComment($id, $login, $text) {
        $user = $this->getUser($login);
        $idUser = $user['id'];
        $query = "insert into bluestore.comments (id_user, id_item, messege) VALUES ($idUser, $id, '$text')";
        $this->getDataQuery($query);
    }

    function addItemBasket($idItem, $idUser) {
        $query = "select * from bluestore.baskets where id_user = $idUser and is_open = 1";
        $res = $this->pdo->query($query);

        $row = $res->fetch(PDO::FETCH_ASSOC);
        $idBasket = -1;
        if (empty($row)) {
            $query = "select * from bluestore.baskets order by id asc limit 1";
            $res = $this->pdo->query($query);
            $row = $res->fetch(PDO::FETCH_ASSOC);
            $idBasket = $row['id'];
            $idBasket++;

            $query = "insert into bluestore.baskets (id, id_user) values ($idBasket, $idUser)";
            $this->pdo->query($query);
        }
        else {
            $idBasket = $row['id'];
        }

        $_SESSION['idBasket'] = $idBasket;
        setcookie('idBasket', $idBasket, '/', TIME_COOKIE);

        $query = "select * from bluestore.link_basket_iems where id_item = $idItem AND id_basket = $idBasket;";
        $res = $this->pdo->query($query);
        $row = $res->fetch(PDO::FETCH_ASSOC);
        $countItem = 1;

        if (empty($row)) {
            $query = "insert into bluestore.link_basket_iems (id_basket, id_item, count) VALUES ($idBasket, $idItem, $countItem)";
            $this->pdo->query($query);
            return;
        }

        $countItem += intval($row['count']);
        $idLink = $row['id'];
        $query = "update bluestore.link_basket_iems set count = $countItem where id = $idLink;";
        $this->pdo->query($query);
    }

    function addFavorite($idUser, $idItem) {
        try {
            $query = "select * from bluestore.favorits where id_user = $idUser and id_item = $idItem;";
            $res = $this->pdo->query($query);
            $row =  $res->fetch(PDO::FETCH_ASSOC);

            if (!empty($row)) {
                return false;
            }

            $query = "insert into bluestore.favorits (id_user, id_item) values ($idUser, $idItem)";
            $this->pdo->query($query);
            return true;
        }
        catch (PDOException $e) {
            print_r($e->getMessage());
        }

        return false;
    }

    function removeFavorite($idUser, $idItem) {
        $query = "delete from bluestore.favorits where id_item = $idItem and id_user = $idUser";
        $this->pdo->query($query);
    }

    function changeCount($idItem, $idBasket, $action) {
        $query = "select * from bluestore.link_basket_iems where id_basket = $idBasket and id_item = $idItem;";
        $res = $this->pdo->query($query);
        $row = $res->fetch(PDO::FETCH_ASSOC);

        if (empty($row)) {
            return;
        }

        $count = intval($row['count']);

        switch ($action) {
            case 'plus':
                $count++;
                break;
            case 'minus':
                $count--;
                break;
            default:
                return;
                break;
        }

        $idLink = $row['id'];
        $query = "update bluestore.link_basket_iems set count = $count where id = $idLink";
        $this->pdo->query($query);
    }

    function deleteItemBasket($idItem, $idBasket) {
        $query = "delete from bluestore.link_basket_iems where id_item = $idItem and id_basket = $idBasket";
        $this->pdo->query($query);
    }
}