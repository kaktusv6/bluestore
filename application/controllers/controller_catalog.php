<?php
class Controller_Catalog extends Controller {
    function action_article($id) {
        if (isset($_POST['send-msg']) && !empty($_POST['send-msg'])) {
            $this->model->addComment($id, $_SESSION['login'], $_POST['text']);
        }

        $data['item'] = $this->model->getItem($id);
        $data['isAdmin'] = $this->isAdmin();
        $data['isAuth'] = $this->isAuthUser();
        $data['comments'] = $this->model->getComments($id);

        foreach($data['comments'] as $i => $comment) {
            $user = $this->model->getUser(intval($comment['id_user']));
            $data['comments'][$i]['name'] = $user['name'];
        }
        $this->view->generate('article', $data);
    }

    function action_article_comments() {

        $this->view->generate('article_comments');
    }

    function action_article_preview() {
        $data = array();

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $item = $this->model->getItem($_GET['id']);
            $data['item'] = $item;
        }
        else {
            $this->locationTo();
        }

        $this->view->generate('article_preview', $data);
    }

    function action_index($id = 0) {
        $data = array();

        if ($id) {
            $data['items'] = $this->model->getItems($id);
        }

        $data['favorites'] = array();

        if (isset($_SESSION['login']) && !empty($_SESSION['login'])) {
            $user = $this->model->getUser($_SESSION['login']);
            $data['favorites'] = $this->model->getFavoritsUser($user['id']);
        }

        $this->view->generate('index', $data);
    }

    function action_add_basket() {
        if (isset($_POST['id_item']) && !empty($_POST['id_item'])) {
            $user = $this->model->getUser($_SESSION['login']);
            $this->model->addItemBasket($_POST['id_item'], $user['id']);
        }
    }

    function action_add_favorite() {
        if (isset($_POST['id_item']) && !empty($_POST['id_item'])) {
            $user = $this->model->getUser($_SESSION['login']);
            $this->model->addFavorite($user['id'], $_POST['id_item']);
        }
    }

    function action_change_basket_item() {
        if (isset($_POST['idItem']) && !empty($_POST['idItem'])) {
            $this->model->changeCount(intval($_POST['idItem']), intval($_POST['idBasket']), $_POST['action']);
        }
    }

    function action_delete_item_basket() {
        if (isset($_POST['idItem']) && !empty($_POST['idItem'])) {
            $this->model->deleteItemBasket(intval($_POST['idItem']), intval($_POST['idBasket']));
        }
    }

    function action_remove_favorite() {
        if (isset($_POST['id_item']) && !empty($_POST['id_item'])) {
            $user = $this->model->getUser($_SESSION['login']);
            $this->model->removeFavorite($user['id'], $_POST['id_item']);
        }
    }
}