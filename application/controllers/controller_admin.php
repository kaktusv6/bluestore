<?php
class Controller_Admin extends Controller {
    function action_index() {
        if (!$this->isAdmin()) {
            $this->locationTo();
        }

        if (isset($_POST['changeUsers']) && !empty($_POST['changeUsers'])) {
            $users = $this->model->getUsers();
            foreach($users as $user) {
                if ($user['status'] != $_POST['curStatus_' . $user['id']]) {
                    $this->model->changeStatus($user['login'], $_POST['curStatus_' . $user['id']]);
                }
            }
            $this->locationTo('admin');
        }

        $data = array();
        $data['users'] = $this->model->getUsers();
        $this->view->generate('index', $data);
    }

    function action_orders() {
        $data = array();

        $this->view->generate('orders', $data);
    }

    function action_articles() {
        $data = array(
            'error' => array()
        );

        if (isset($_POST['user-login']) && !empty($_POST['user-login'])) {
            /* POST
                name
                desc
                price
                category
                count
                user-login
             * */

            $dataItem = $_POST;
            foreach($_FILES as $file) {
                $result = $this->model->addImage($file);
                if ($result['isUpload']) {
                    $dataItem['images'][] = $result['path_image'];
                }
            }

            if (empty($dataItem['images'])) {
                $dataItem['images'][] = DEFAULT_IMAGE;
            }

            $isError = false;
            foreach($_POST as $key => $input) {
                if (empty($input)) {
                    $data['error'][$key] = true;
                    $isError = true;
                }
            }
            if (!$isError) {
                $this->model->addItem($dataItem);
            }
        }

        $this->view->generate('articles', $data);
    }

    function action_order_details() {
        $data = array();

        $this->view->generate('order_details', $data);
    }

    function action_user($id) {

        $data = array();
        $data['user'] = $this->model->getUserBuId($id);
        $this->view->generate('user', $data);
    }

    function action_article($id = 0) {
        if ($id == 0 || !$this->isAdmin()) {
            $this->locationTo();
        }

        $data = array();
        $data['isSuccess'] = true;
        $data['message'] = "";

        if (isset($_POST['user-login-save']) && !empty($_POST['user-login-save'])) {
            $dataItem = $_POST;
            foreach($_FILES as $file) {
                $result = $this->model->addImage($file);
                if ($result['isUpload']) {
                    $dataItem['images'][] = $result['path_image'];
                }
            }

            $res = $this->model->changeDataItem($id, $dataItem);

            if (!$res['isSuccess']) {
                $data['isSuccess'] = false;
                $data['message'] = $res['message'];
            }
        }

        $data['item'] = $this->model->getItem($id);

        $data['images'] = array($data['item']['path_image'], $data['item']['path_image_2'], $data['item']['path_image_3']);
        foreach ($data['images'] as $id => $image) {
            if (isset($image) && !empty($image)) {
                $str = explode("/", $image);
                $data['images'][$id] = $str[2];
            }
            else {
                $data['images']['id'] = "Выберите файл";
            }
        }

        $this->view->generate('article', $data);
    }
}