<?php
class Controller_Main extends Controller {
    function action_index()
    {
        $data = array();
        $data['items'] = $this->model->getCountItems(5);
        $data['favorites'] = array();

        if (isset($_SESSION['login']) && !empty($_SESSION['login'])) {
            $user = $this->model->getUser($_SESSION['login']);
            $data['favorites'] = $this->model->getFavoritsUser($user['id']);
        }
        $this->view->generate('index', $data);
    }
}