<?php

class Controller_User extends Controller {

    function action_favorites()
    {
        $data = array();
        $user = $this->model->getUser($_SESSION['login']);
        $data['favorits'] = $this->model->getFavoritsUser($user['id']);

        $this->view->generate('favorites', $data);
    }

    function action_login()
    {
        if ($this->isAuthUser()) {
            $this->locationTo('user');
        }

        $data = array();

        if (isset($_POST['user-login']) && !empty($_POST)) {
            $users = $this->model->getUsers();
            $isAuth = false;
            foreach($users as $user) {
                if ($user['login'] == $_POST['email'] && $user['password'] == md5($_POST['password'])) {
                    $isAuth = true;
                    setcookie('login', $_POST['email'], time() + TIME_COOKIE, '/');
                    setcookie('isAdmin', $user['status'], time() + TIME_COOKIE, '/');
                    $_SESSION['login'] = $_POST['email'];
                    $_SESSION['isAdmin'] = $user['status'];
                }
            }

            if (!$isAuth) {
                $data['isError'] = true;
                $data['msgError'] = "Вы ввели неправильно логин/пароль";
            }
            else {
                $this->locationTo('user');
            }
        }

        if (isset($_POST['user-registration']) && !empty($_POST['user-registration'])) {
            $users = $this->model->getUsers();
            $isValid = true;
            foreach($users as $user) {
                if ($user['login_email'] == $_POST['email']) {
                    $isValid = false;
                    $data['isError'] = true;
                    $data['msgError'] = 'Пользователь с таким E-mail уже зарегестрирован';
                    $data['data'] = $_POST;
                }
            }

            if ($isValid) {
                $this->model->registerUser($_POST);
                setcookie('login', $_POST['email'], time() + TIME_COOKIE, '/');
                setcookie('isAdmin', $user['status'], time() + TIME_COOKIE, '/');
                $_SESSION['login'] = $_POST['email'];
                $_SESSION['isAdmin'] = $user['status'];

                $this->locationTo('user');
            }
        }

        $this->view->generate('login', $data);
    }

    function action_login_si() {
        $data = array();

        $this->view->generate('login_si', $data);
    }

    function action_login_reg() {
        $data = array();

        $this->view->generate('login_reg', $data);
    }

    function action_registration()
    {
        if ($this->isAuthUser()) {
            $this->locationTo('user');
        }

        $data = array();
        if (isset($_POST['user-registration']) && !empty($_POST['user-registration'])) {
            $users = $this->model->getUsers();
            $isValid = true;
            foreach($users as $user) {
                if ($user['login_email'] == $_POST['email']) {
                    $isValid = false;
                    $data['isError'] = true;
                    $data['msgError'] = 'Пользователь с таким E-mail уже зарегестрирован';
                    $data['data'] = $_POST;
                }
            }

            if ($isValid) {
                $this->model->registerUser($_POST);
                setcookie('login', $_POST['email'], time() + TIME_COOKIE, '/');
                setcookie('isAdmin', $user['status'], time() + TIME_COOKIE, '/');
                $_SESSION['login'] = $_POST['email'];
                $_SESSION['isAdmin'] = $user['status'];

                $this->locationTo('user');
            }
        }

        $this->view->generate('registration', $data);
    }

    function action_order()
    {

        $this->view->generate('order');
    }

    function action_ordering()
    {

        if (!isset($_POST['items']) || empty($_POST['items'])) {
            $this->locationTo('user', 'basket');
        }

        $data = array();
        $data['isError'] = false;
        $data['message'] = "";
        if (isset($_POST['user-login']) && !empty($_POST['user-login'])) {
            if ($data['isError'] = $_POST['delivery-type'] == -1) {
                $data['message'] = 'Выберите тип доставки';
            }
            else if ($data['isError'] = $_POST['delivery-address'] == -1 && $_POST['delivery-type'] == 1) {
                $data['message'] = 'Выберите магазин для самовывоза';
            }
            else {
                $user = $this->model->getUser($_SESSION['login']);
                $idBasket = $_SESSION['idBasket'];
                $res = $this->model->checkoutOrder($user['id'], $idBasket);
                if ($res['isSuccess']) {
                    $this->locationTo('user', 'order');
                }

                $data['isError'] = true;
                $data['message'] = $res['message'];
            }
        }

        $data['items'] = $_POST['items'];

        $data['user'] = $this->model->getUser($_SESSION['login']);

        $this->view->generate('ordering', $data);
    }

    function action_order_details()
    {

        $this->view->generate('order_details');
    }

    function action_basket()
    {
        if (!$this->isAuthUser()) {
            $this->locationTo();
        }

        $data = array();
        $user = $this->model->getUser($_SESSION['login']);

        $data['items'] = $this->model->getItemsBasket($user['id']);

        $this->view->generate('basket', $data);
    }

    function action_refactor()
    {
        if (!$this->isAuthUser()) {
            $this->locationTo();
        }

        if (isset($_POST['user-login']) && !empty($_POST['user-login'])) {
            $this->model->changeDataUser($_SESSION['login'], $_POST);
        }

        $data = array();
        $data = $this->model->getUser($_SESSION['login']);

        $this->view->generate('refactor', $data);
    }

    function action_index()
    {
        if (!$this->isAuthUser()) {
            $this->locationTo();
        }

        if (isset($_GET['logout']) && $_GET['logout'] == 'Y') {
            $this->logout();
            $this->locationTo();
        }

        $data = array();
        $data = $this->model->getUser($_SESSION['login']);

        $this->view->generate('index', $data);
    }
}