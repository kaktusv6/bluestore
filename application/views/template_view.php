<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<title>Основная страница магазина</title>

    <link rel="stylesheet" type="text/css" href="/css/template_styles.css" />
    <link rel="stylesheet" type="text/css" href="/css/top-menu.css" />
    <link rel="stylesheet" type="text/css" href="/css/content_main.css" />
    <link rel="stylesheet" type="text/css" href="/css/content_catalog.css" />
    <link rel="stylesheet" type="text/css" href="/css/content_user.css" />
    <link rel="stylesheet" type="text/css" href="/css/content_admin.css" />
    <link rel="stylesheet" type="text/css" href="/css/article_preview.css" />
    <link rel="stylesheet" type="text/css" href="/css/slider_style.css" />
    <link rel="stylesheet" type="text/css" href="/css/lightslider.css" />
    <link rel="stylesheet" type="text/css" href="/css/lightgallery.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<body>

<!-- Header -->
<header class="header">
        <div class="lk">
            <ul class="user-menu-ul">
                <?php if(isset($_SESSION['isAdmin']) && $_SESSION['isAdmin']):?>
                <li class="user-menu-li">
                        <a href="/admin/orders" class="header-lk">
                            <div class="user-menu-li-img"></div>
                            Администратор
                        </a>
                        <ul class="user-sub-menu">
                            <li class="user-sub-menu-li"><a href="/admin/" class="header-lk">Пользователи</a></li>
                            <li class="user-sub-menu-li"><a href="/admin/orders" class="header-lk">Список заказов</a></li>
                            <li class="user-sub-menu-li"><a href="/admin/articles" class="header-lk">Товары</a></li>
                        </ul>
                </li>
                <?php endif;?>

                <li class="user-menu-li">
                    <?php if (isset($_SESSION['login'])):?>
                        <a href="/user/" class="header-lk">
                            <div class="user-menu-li-img"></div>
                            <?php echo $_SESSION['login']?>
                        </a>
                        <ul class="user-sub-menu">
                                <li class="user-sub-menu-li-lk"><a href="/user/" class="header-lk">Профиль</a></li>
                                <li class="user-sub-menu-li-lk"><a href="/user/?logout=Y" class="header-lk">Выйти</a></li>
                            </ul>
                    <?php else:?>
                        <a href="/user/login" class="header-lk">Вход / Регистрация</a>
                    <?php endif;?>
                </li>
                <li><a href="/user/basket" class="header-lk">Корзина</a></li>
                <li><a href="/user/favorites" class="header-lk">Избранное</a></li>
            </ul>
        </div>
    <div id="logo"><a href="/" class="header-main">BlueStore</a></div>
    <div class="clear"></div>
    <div class="header-top-menu-div">
        <nav id="header-top-menu" style="top:65px">
            <div class="header-top-menu-div2" ></div>
            <ul id="top-menu">
<!--                <li><a href="/catalog/">Каталог</a></li>-->
                <?php foreach($data['categories'] as $category):?>
                    <li><a href="/catalog/<?php echo $category['id']?>/"><?php echo $category['name']?></a></li>
                <?php endforeach;?>
<!--                <li><a href="/catalog/article_preview">article_preview</a></li>-->
<!--                <li><a href="/user/">ЛК</a></li>-->
            </ul>
        </nav>
    </div>

    <div class="clear"></div>
</header>
<!-- / Header -->

<div class="gap"></div>

<!-- Main Content -->
<main class="wrapper">
    <div class="content">
        <div class="clear"></div>

        <div class="modal" id="myModal">
            <div class="modal-content">
                <span class="close_modal">&times;</span>
                <div id="modal_preview_content">   </div>
            </div>
        </div>

<!-- Include content action -->
		<?php
			$controller = $this->controller->get_name_controller();
			include "application/views/$controller/$action.php";
		?>
<!-- / Include content action -->
    </div>
<div class="clear"></div>
</main>
<!-- / Main Content -->

<div class="gap"></div>

<!-- Footer -->
<footer class="footer">
    <ul id="menu-footer">
        <li><a href="#">Контакты</a></li>
        <li><a href="#">Что-нибудь</a></li>
        <li><a href="#">О Нас</a></li>
        <li><a href="#">Еще что-нибудь </a></li>
    </ul>
</footer>
<!-- / Footer -->
<script>
    $(document).ready(function() {
        <?php
        if (isset($_GET['jscode']) && !empty($_GET['jscode'])) {
            echo $_GET['jscode'];
        }
        ?>
    });
</script>
<script src="/scripts/lightslider.js"></script>
<script src="/scripts/lightgallery.js"></script>
<script src="/scripts/header_fix.js"></script>
<script src="/scripts/slider_main.js"></script>
<script src="/scripts/validate.js"></script>
<script src="/scripts/slider_item.js"></script>
<script src="/scripts/input_change.js"></script>
<script src="/scripts/maskedinput.js"></script>
<script src="/scripts/listener_basket.js"></script>
<script src="/scripts/jquery-ui.js"></script>
<script src="/scripts/ajax_artilce_preview.js"></script>
<script src="/scripts/ajax_artilce_preview.js"></script>
<script src="/scripts/login_reg.js"></script>
<script src="/scripts/inLike.js"></script>
<script src="/scripts/change_file.js"></script>
</head>
</body>
</html>
