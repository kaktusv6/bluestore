<?php
//pp($data);
$user = $data['user'];
$items = $data['items'];
?>

<div class="user-left">
    <div class="user-logo"></div>
    <a href="/user/"><button class="user-menu-btn btn">Профиль</button></a>
    <a href="/user/order"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/user/favorites"><button class="user-menu-btn btn">Избранное</button></a>
    <a href="/user/basket"><button class="user-menu-btn btn btn-active">Корзина</button></a>
</div>

<div class="user-right">
    <div class="user-right-content">
        <div class="user-refactor">
            <form method="post">
                <div class="user-ordering-div">
                    <?php foreach($items as $item):?>
                        <input type="hidden" name="items[]" value="<?php echo $item?>">
                    <?php endforeach;?>
                    <span class="user-ordering-span">Личные данные</span>
                    <input name="login" type="email" class="user-refactor-input" value="<?php echo $user['login']?>" placeholder="Введите Ваш E-mail">
                    <input name="name" type="text" class="user-refactor-input" value="<?php echo $user['name']?>" placeholder="Ваше имя">
                    <input name="tel" type="text" class="user-refactor-input" value="<?php echo $user['tel']?>" placeholder="Введите телефон">
                </div>

                <div class="user-ordering-div2">
                    <span class="user-ordering-span">Данные доставки</span>

                    <select class="user-ordering-type" name="delivery-type">
                        <option value="-1">
                            Выберете способ доставки
                        </option>
                        <option value="1">Самовывоз</option>
                        <option value="2">Доставка</option>
                    </select>


                    <div class="user-ordering-div3">
                        <input name="country" type="text" class="user-refactor-input" value="<?php echo $user['country']?>" placeholder="Страна">
                        <input name="city" type="text" class="user-refactor-input" value="<?php echo $user['city']?>" placeholder="Город">
                        <input name="address" type="text" class="user-refactor-input" value="<?php echo $user['address']?>" placeholder="Адрес">
                    </div>

                    <select class="user-ordering-type" name="delivery-address">
                        <option value="-1">
                            Выберете точку самовывоза
                        </option>
                        <option value="1">На Окатовой</option>
                        <option value="2">На Светланской</option>
                    </select>
                </div>

                <div class="clear"></div>
                <input type="submit" name="user-login" class="btn user-info-btn" value="Оформить">
            </form>
        </div>
    </div>

        </div>
    </div>
</div>

<?php if($data['isError']):?>
    <script>
        $(document).ready(function () {
            alert("<?php echo $data['message']?>");
        });
    </script>
<?php endif;?>