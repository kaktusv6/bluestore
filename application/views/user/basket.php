<div class="user-left">
    <div class="user-logo"></div>
    <a href="/user/"><button class="user-menu-btn btn">Профиль</button></a>
    <a href="/user/order"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/user/favorites"><button class="user-menu-btn btn">Избранное</button></a>
    <a href="/user/basket"><button class="user-menu-btn btn btn-active">Корзина</button></a>
</div>
<form method="post" action="/user/ordering/">
<div class="user-right">
    <div class="user-right-content">
        <div class="user-order-basket-name">
            <div class="user-order-basket-name-div">
            <span class="user-order-details-span"> товар </span>
            <span class="user-order-details-span"> цена </span>
            <span class="user-order-details-span"> кол-во </span>
            <span class="user-order-details-span"> сумма </span>
            </div>
        </div>

        <div class="user-order-info">
            <?php $sum = 0;?>
            <?php foreach ($data['items'] as $id => $item): ?>
                <input type="hidden" name="items[]" value="<?php echo $item['id']?>">
                <?php
                $sumItem = intval($item['price']) * intval($item['count']);
                $sum += $sumItem;
                ?>
                <a class="user-order-basket-a" href="/catalog/article/<?php echo $item['id']?>/">
                    <div class="user-basket" >
                        <span class="user-order-basket-span"> <?php echo $item['name'] ?> </span>
                        <span class="user-order-basket-span"> <?php echo $item['price'] ?> руб </span>
                        <span class="user-order-basket-span">
                            <div class="user-basket-number-div">
                                <script type="text/javascript">
                                    var price_<?php echo $item['id']?> = <?php echo $item['price'] ?>;
                                </script>
                                <button class="minus btn minus-plus" onclick="btn_minus(price_<?php echo $item['id']?>, '<?php echo $item["id"]?>'); return false;">-</button>
                                <input id="count_<?php echo $item['id']?>" type="number" min="0" max="+50" step="any" value="<?php echo $item['count']?>" class="user-refactor-number" onchange="sum_item(price_<?php echo $item['id']?>, this, 'item_sum<?php echo $id ?>')">
                                <button class="plus btn minus-plus" onclick="btn_plus(price_<?php echo $item['id']?>, '<?php echo $item["id"]?>'); return false;">+</button>
                            </div>
                        </span>
                        <div class="user-order-basket-span">
                            <span id="item_sum<?php echo $id ?>"> <?php echo $sumItem ?> руб</span>
                        </div>

                    </div>
                </a>
                <button class="user-basket-btn" onclick="deleteItem(<?php echo $item['id']?>)"></button>
            <?php endforeach; ?>
        </div>
        <script> var all_sum = <?php echo $sum; ?></script>
        <div class="user-order-basket-total">
            <div class="user-order-basket-span-total">
                <span id="total_sum"><?php echo $sum?> руб</span>
            </div>
            <span class="user-order-basket-span-total"> Товаров на сумму </span>
        </div>
        <div class="clear"></div>
<!--        <a href="/user/ordering" style="text-decoration: none">-->
            <button class="btn btn-basket"> Оформить заказ </button>
<!--        </a>-->
    </div>
</div>
</form>
<script>
    var idBasket = <?php echo $_SESSION['idBasket']?>
</script>