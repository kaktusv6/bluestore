<?php //pp($data)?>

<div class="user-left">
    <div class="user-logo"></div>
    <a href="/user/"><button class="user-menu-btn btn">Профиль</button></a>
    <a href="/user/order"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/user/favorites"><button class="user-menu-btn btn btn-active">Избранное</button></a>
    <a href="/user/basket"><button class="user-menu-btn btn">Корзина</button></a>
</div>


<div class="user-right">
    <div class="user-right-content">
<!--        <div class="user-favorites">-->
            <?php foreach ($data['favorits'] as $item): ?>
                <div class="mini-article-f">
                    <div class="mini-article-logo">
                        <button class="mini-article-button-info" onclick='dialogOnClick("/catalog/article_preview/?id=<?php echo $item['id']?>");'>Предпросмотр</button>
                        <a href="/catalog/article/<?php echo $item['id']?>"><img src="<?php echo $item['path_image']?>" class="mini-article-logo-img"></a>
                    </div>
                    <div class="mini-article-info-f">
                        <button class="mini-article-favorites mini-article-favorites-active"></button>
                        <div class="mini-article-price-div"><span class="mini-article-price"><?php echo $item['price']?> руб.</span></div>
                        <button class="mini-article-basket" onclick="add_basket(<?php echo $item['id']?>)"></button>
                    </div>
                </div>
            <?php endforeach; ?>
<!--        <button class="btn mini-article-button">Показать еще</button>-->
<!--        <div class="catalog-page-info"><span>1 из 1</span></div>-->
<!--        </div>-->
        <div class="clear"></div>
    </div>

</div>