<div class="user-left">
    <div class="user-logo"></div>
    <a href="/user/"><button class="user-menu-btn btn btn-active">Профиль</button></a>
    <a href="/user/order"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/user/favorites"><button class="user-menu-btn btn">Избранное</button></a>
    <a href="/user/basket"><button class="user-menu-btn btn">Корзина</button></a>


</div>

<div class="user-right">
    <div class="user-right-content">
        <div class="user-info">
            <h2 class="user-info-h2">Личные данные</h2>

            <div class="user-info-left">
                <span class="user-info-left-span user-info-span"> Имя </span>

                <span class="user-info-left-span user-info-span"> Фамилия </span>

                <span class="user-info-left-span user-info-span"> e-mail </span>

                <span class="user-info-left-span user-info-span"> Телефон </span>
            </div>
            <div class="user-info-right">
                <span class="user-info-right-span user-info-span"> <?php echo($data['name']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($data['second_name']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($data['login']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($data['tel']) ?> </span>
            </div>

            <h2 class="user-info-h2">Данные доставки</h2>

            <div class="user-info-left">
                <span class="user-info-left-span user-info-span"> Страна </span>

                <span class="user-info-left-span user-info-span"> Город </span>

                <span class="user-info-left-span user-info-span"> Адрес </span>
            </div>
            <div class="user-info-right">
                <span class="user-info-right-span user-info-span"> <?php echo($data['country']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($data['city']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($data['address']) ?> </span>
            </div>

            <a href="/user/refactor" style="text-decoration: none"><button class="user-info-btn btn"> Изменить </button></a>
            
        </div>

    </div>


</div>