<div class="user-left">
    <div class="user-logo"></div>
    <a href="/user/"><button class="user-menu-btn btn btn-active">Профиль</button></a>
    <a href="/user/order"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/user/favorites"><button class="user-menu-btn btn">Избранное</button></a>
    <a href="/user/basket"><button class="user-menu-btn btn">Корзина</button></a>


</div>

<div class="user-right">
    <div class="user-right-content">
        <div class="user-refactor">
            <form method="post" name="refactor" onsubmit='return validate_refactor()'>
                <div class="user-refactor-left">
                    <span class="user-refactor-span">Изменение личных данных</span>
<!--                    <input name="email" type="email" class="user-refactor-input" value="--><?php //echo $data['login'] ?><!--" placeholder="Введите Ваш E-mail">-->

                    <input name="name" type="text" class="user-refactor-input" value="<?php echo $data['name'] ?>" placeholder="Ваше имя">

                    <input name="second_name" type="text" class="user-refactor-input" value="<?php echo $data['second_name'] ?>" placeholder="Ваша фамилия">

                    <input name="phone" id="phone" type="text" class="user-refactor-input" value="<?php echo $data['tel'] ?>" placeholder="Введите телефон">

                    <input name="country" type="text" class="user-refactor-input" value="<?php echo $data['country'] ?>" placeholder="Страна">

                    <input name="city" type="text" class="user-refactor-input" value="<?php echo $data['city'] ?>" placeholder="Город">

                    <input name="address" type="text" class="user-refactor-input" value="<?php echo $data['address'] ?>" placeholder="Адрес">
                </div>
                <div class="user-refactor-right">
                    <span class="user-refactor-span">Изменение пароля</span>

                    <input name="password_old" type="password" class="user-refactor-input" placeholder="Введите старый пароль">

                    <input name="password_new" type="password" class="user-refactor-input" placeholder="Введите новый пароль">

                    <input name="password_repeat" type="password" class="user-refactor-input" placeholder="Введите пароль еще раз">

                </div>
                <input type="submit" name="user-login" class="btn user-info-btn" value="Сохранить">

            </form>
            
        </div>

    </div>


</div>