<div class="user-login-window">
    <div class="user-login-name">
        <button class="btn user-login-name-button btn-active" onclick="login_enter()">Авторизация</button>
        <button class="btn user-login-name-button" onclick="reg_enter()">Регистрация</button>
    </div>
    <div class="user-login-window-content">

        <span class="user-login-error">
            <?php
            if (isset($data['isError']) && !empty($data['isError'])) {
                echo $data['msgError'];
                $data['isError'] = false;
            }
            ?>
        </span>

    <div class="user-login" id="login_reg">

        
    </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        login_enter();
    });
</script>