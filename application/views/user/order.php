<div class="user-left">
    <div class="user-logo"></div>
    <a href="/user/"><button class="user-menu-btn btn">Профиль</button></a>
    <a href="/user/order"><button class="user-menu-btn btn btn-active">Заказы</button></a>
    <a href="/user/favorites"><button class="user-menu-btn btn">Избранное</button></a>
    <a href="/user/basket"><button class="user-menu-btn btn">Корзина</button></a>
</div>

<div class="user-right">
    <div class="user-right-content">
            <h2 class="user-info-h2">История заказов</h2>
        <div class="admin-orders-filter">
            <form method="post">
                <span class="admin-orders-filter-span"> Заказы с даты</span>
                <input type="date" class="admin-orders-filter-date">
                <span class="admin-orders-filter-span"> Заказы со статусом</span>
                <select class="admin-order-status">
                    <option>
                        <!--                        Здесь должен отражаться текущий статус заказа-->
                        Статус заказа
                    </option>
                    <option value="1">принят</option>
                    <option value="2">отправлен</option>
                    <option value="3">доставлен</option>
                    <option value="4">выполнен</option>
                </select>
                <input type="submit" value="Применить" class="btn btn-order-admin">
            </form>

        </div>
        <div class="user-order">
<!--        /* повторяющаяся часть */-->
        <a href="/user/order_details">
            <button class="user-order-btn" >
                <span class="user-order-span"> 000 </span>
                <span class="user-order-span"> 01.11.2002 </span>
                <span class="user-order-span"> выполнено </span>
            </button>
        </a>
<!--        /* конец повторяющейся части */-->
        </div>
    </div>
</div>