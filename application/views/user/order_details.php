<div class="user-left">
    <div class="user-logo"></div>
    <a href="/user/"><button class="user-menu-btn btn">Профиль</button></a>
    <a href="/user/order"><button class="user-menu-btn btn btn-active">Заказы</button></a>
    <a href="/user/favorites"><button class="user-menu-btn btn">Избранное</button></a>
    <a href="/user/basket"><button class="user-menu-btn btn">Корзина</button></a>


</div>

<div class="user-right">
    <div class="user-right-content">
        <h2 class="user-info-h3"> Заказ номер 000 </h2>
        <h4 class="user-info-h4"> 01.11.2002 </h4>
        <div class="user-order-details-total">
            <span class="user-order-details-span-total"> 1000 руб </span>
            <span class="user-order-details-span-total"> Сумма заказа </span>
        </div>
        <div class="clear"></div>
        <div class="gap"></div>
        <div class="user-order-details-name" >
            <span class="user-order-details-span"> товар </span>
            <span class="user-order-details-span"> цена </span>
            <span class="user-order-details-span"> кол-во </span>
            <span class="user-order-details-span"> сумма </span>
        </div>
        <div class="user-order-info">


<!--        /* повторяющаяся часть */-->
            <?php for ($i=0; $i<12; $i++){ ?>
            <a class="user-order-details-a" href="/catalog/">
                <div class="user-order-details" >
                    <span class="user-order-details-span"> товар <?php echo $i ?> </span>
                    <span class="user-order-details-span"> 500 руб </span>
                    <span class="user-order-details-span"> 2 </span>
                    <span class="user-order-details-span"> 1000 руб </span>
                </div>
            </a>
            <?php } ?>
<!--        /* конец повторяющейся части */-->


        </div>

        <button class="btn btn-order"> Статус / Отменить заказ </button>

    </div>


</div>