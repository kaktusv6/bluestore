<!-- начало слайдера -->
<div id="block-for-slider">
    <div id="viewport">
        <ul id="slidewrapper">
            <li class="slide">
                <!-- <img src="/images/1.png" alt="1" class="slide-img"> -->
                <div class="slider-img" style="background-image: url(/images/1.png);"></div>
            </li>
            <li class="slide">
                <!-- <img src="/images/2.jpg" alt="2" class="slide-img"> -->
                <div class="slider-img" style="background-image: url(/images/2.jpg);"></div>
            </li>
        </ul>

        <div id="prev-next-btns">
            <div id="prev-btn"></div>
            <div id="next-btn"></div>
        </div>

        <ul id="nav-btns">
                <li class="slide-nav-btn"></li>
                <li class="slide-nav-btn"></li>
            </ul>
    </div>
</div>
<!-- конец слайдера -->

<div class="gap"></div>


<?php //pp($data)?>
<div class="article-top">
    <?php foreach ($data['items'] as $item): ?>
        <?php
        $isFavorite = false;
        foreach($data['favorites'] as $favorite) {
            if ($favorite['id'] == $item['id']) {
                $isFavorite = true;
                break;
            }
        }
        ?>
        <div class="mini-article">

            <div class="mini-article-logo">
                <button class="mini-article-button-info" onclick="modal_preview('/catalog/article_preview/?id=<?php echo $item['id']?>');">Предпросмотр</button>
                <a href="/catalog/article/<?php echo $item['id']?>"><img src="<?php echo $item['path_image']?>" class="mini-article-logo-img"></a>
            </div>

            <div class="mini-article-info">
                <?php if ($isFavorite):?>
                    <button class="mini-article-favorites mini-article-favorites-active" onclick="dislike(<?php echo $item['id']?>)"></button>
                <?php else:?>
                    <button class="mini-article-favorites" onclick="like(<?php echo $item['id']?>)"></button>
                <?php endif;?>
                <div class="mini-article-price-div"><span class="mini-article-price"><?php echo $item['price']?> руб.</span></div>
                <button class="mini-article-basket" onclick="add_basket(<?php echo $item['id']?>)"></button>
            </div>

        </div>
    <?php endforeach; ?>

</div>


