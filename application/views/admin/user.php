<div class="user-left">
    <div class="admin-logo"></div>
    <a href="/admin/"><button class="user-menu-btn btn btn-active">Пользователи</button></a>
    <a href="/admin/orders"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/admin/articles"><button class="user-menu-btn btn">Товары</button></a>
</div>

<div class="user-right">
    <div class="user-right-content">
        <div class="user-info">
            <h2 class="user-info-h2">Личные данные</h2>

            <div class="user-info-left">
                <span class="user-info-left-span user-info-span"> Имя </span>

                <span class="user-info-left-span user-info-span"> Фамилия </span>

                <span class="user-info-left-span user-info-span"> e-mail </span>

                <span class="user-info-left-span user-info-span"> Телефон </span>
            </div>
            <?php
            $user = $data['user'][0];
            ?>
            <div class="user-info-right">
                <span class="user-info-right-span user-info-span"> <?php echo($user['name']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($user['second_name']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($user['login']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($user['tel']) ?> </span>
            </div>

            <h2 class="user-info-h2">Данные доставки</h2>

            <div class="user-info-left">
                <span class="user-info-left-span user-info-span"> Страна </span>

                <span class="user-info-left-span user-info-span"> Город </span>

                <span class="user-info-left-span user-info-span"> Адрес </span>
            </div>
            <div class="user-info-right">
                <span class="user-info-right-span user-info-span"> <?php echo($user['country']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($user['city']) ?> </span>

                <span class="user-info-right-span user-info-span"> <?php echo($user['address']) ?> </span>
            </div>

        </div>
    </div>
</div>