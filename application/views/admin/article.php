<?php
//pp($data);
?>

<div class="user-left">
    <div class="admin-logo"></div>
    <a href="/admin/"><button class="user-menu-btn btn">Пользователи</button></a>
    <a href="/admin/orders"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/admin/articles"><button class="user-menu-btn btn btn-active">Товары</button></a>
</div>

<style>
    .error {
        border-color: red;
    }
</style>

<div class="user-right">
    <div class="user-right-content">
        <span class="admin-articles-span">Добавление нового товара</span>
        <form method="post" enctype="multipart/form-data">
            <input name="name" type="text" class="<?php echo (isset($data['error']['name']) ? 'error' : '') ?> admin-articles-input" value="<?php echo (!empty($data['error']) ? $_POST['name'] : $data['item']['name']) ?>" placeholder="Введите название товара">
            <textarea name="desc" id="" cols="30" rows="5" class="<?php echo isset($data['error']['desc']) ? 'error' : '' ?> admin-articles-textarea" placeholder="Введите описание"><?php echo (!empty($data['error']) ? $_POST['desc'] : $data['item']['description']) ?></textarea>

            <input name="price" type="number" class="<?php echo isset($data['error']['price']) ? 'error' : '' ?> admin-articles-input" value="<?php echo (!empty($data['error']) ? $_POST['price'] : $data['item']['price']) ?>" placeholder="Введите цену">

            <div class="admin-refactor-sn">
                <select class="<?php echo isset($data['error']['category']) ? 'error' : '' ?> admin-refactor-status" name="category">
                    <?php foreach($data['categories'] as $category):?>
                        <option value="<?php echo $category['id'] ?>" <?php echo (!empty($data['error']) ? $category['id'] == $_POST['category'] ? 'selected' : $data['item']['id_category'] : $data['item']['id_category'])?>><?php echo $category['name']?></option>
                    <?php endforeach;?>
                </select>
                <div class="admin-refactor-number-div">
                    <button class="minus btn minus-plus">-</button>
                    <input type="number" name="count" min="0" max="+50" step="any" value="<?php echo (!empty($data['error']) ? $_POST['count'] : $data['item']['count']) ?>" class="<?php echo isset($data['error']['count']) ? 'error' : '' ?> user-refactor-number">
                    <button class="plus btn minus-plus">+</button>
                </div>

            </div>
            <div  class="admin-articles-file">
                <label class="admin-articles-file-label">
                    <input type="file" name="photo_1" class="admin-articles-file-input" id="input_file_1" onchange="change('input_file_1');">
                    <span class="admin-articles-file-span" id="input_file_1_btn"><?php echo $data['images'][0]?></span>
                </label>
            </div>

            <div  class="admin-articles-file">
                <label class="admin-articles-file-label">
                    <input type="file" name="photo_2" class="admin-articles-file-input" id="input_file_2" onchange="change('input_file_2');">
                    <span  class="admin-articles-file-span" id="input_file_2_btn"><?php echo $data['images'][1]?></span>
                </label>
            </div>

            <div  class="admin-articles-file">
                <label class="admin-articles-file-label">
                    <input type="file" name="photo_3" class="admin-articles-file-input" id="input_file_2" onchange="change('input_file_2');">
                    <span  class="admin-articles-file-span" id="input_file_3_btn"><?php echo $data['images'][2]?></span>
                </label>
            </div>
            <input type="submit" name="user-login-save" class="btn user-info-btn" value="Сохранить">

        </form>

    </div>
</div>