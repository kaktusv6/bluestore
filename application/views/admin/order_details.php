<div class="user-left">
    <div class="admin-logo"></div>
    <a href="/admin/"><button class="user-menu-btn btn">Пользователи</button></a>
    <a href="/admin/orders"><button class="user-menu-btn btn btn-active">Заказы</button></a>
    <a href="/admin/articles"><button class="user-menu-btn btn">Товары</button></a>
</div>

<div class="user-right">
    <div class="user-right-content">
        <h2 class="user-info-h3"> Заказ номер 000 от
            <a href="/admin/user">
            <button class="admin-order-details-user">
                tokhtina.as@yandex.ru

            </button>
            </a>
        </h2>
        <h4 class="user-info-h4"> 01.11.2002 </h4>
        <div class="user-order-details-total">
            <span class="user-order-details-span-total"> 1000 руб </span>
            <span class="user-order-details-span-total"> Сумма заказа </span>
        </div>
        <div class="clear"></div>
        <div class="gap"></div>
        <div class="user-order-details-name" >
            <span class="user-order-details-span"> товар </span>
            <span class="user-order-details-span"> цена </span>
            <span class="user-order-details-span"> кол-во </span>
            <span class="user-order-details-span"> сумма </span>
        </div>
        <div class="user-order-info">

<!--        /* повторяющаяся часть */-->
            <?php for ($i=0; $i<12; $i++){ ?>
            <a class="user-order-details-a" href="/catalog/">
                <div class="user-order-details" >
                    <span class="user-order-details-span"> товар <?php echo $i ?> </span>
                    <span class="user-order-details-span"> 500 руб </span>
                    <span class="user-order-details-span"> 2 </span>
                    <span class="user-order-details-span"> 1000 руб </span>
                </div>
            </a>
            <?php } ?>
<!--        /* конец повторяющейся части */-->

        </div>
        <div class="admin-form-status">
            <form method="post">
                <select class="admin-order-status">
                    <option>
<!--                        Здесь должен отражаться текущий статус заказа-->
                        Статус заказа
                    </option>
                    <option value="1">принят</option>
                    <option value="2">отправлен</option>
                    <option value="3">доставлен</option>
                    <option value="4">выполнен</option>
                </select>
                <input type="submit" value="Сохранить" class="btn btn-order-admin">
            </form>
        </div>
    </div>
</div>