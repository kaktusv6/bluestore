<div class="user-left">
    <div class="admin-logo"></div>
    <a href="/admin/"><button class="user-menu-btn btn btn-active">Пользователи</button></a>
    <a href="/admin/orders"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/admin/articles"><button class="user-menu-btn btn">Товары</button></a>


</div>

<div class="user-right">
    <div class="user-right-content">

        <div class="admin-users-name">
            <div class="admin-users-name-div">
                <span class="admin-users-span2 admin-users-span-name"> Имя </span>
                <span class="admin-users-span2 admin-users-span-email"> email </span>
                <span class="admin-users-span2 admin-users-span-phone"> телефон </span>
                <span class="admin-users-span2 admin-users-span-status"> статус </span>
            </div>
        </div>
        
        <form method="post">
            <div class="admin-users-info">
                <?php foreach ($data['users'] as $user): ?>
                    <div class="admin-users" >
                        <a class="admin-index-user-a" href="/admin/user/<?php echo $user['id']?>">
                            <span class="admin-users-span admin-users-span-name"> <?php echo $user['name']?> </span>
                            <span class="admin-users-span admin-users-span-email"> <?php echo $user['login']?> </span>
                            <span class="admin-users-span admin-users-span-phone"> <?php echo $user['tel']?> </span>
                        </a>
                        <?php if($_SESSION['login'] != $user['login']):?>
                        <select class="admin-users-status" name="curStatus_<?php echo $user['id']?>">
                            <?php if ($user['status']):?>
                                <option value="1">Администратор</option>
                                <option value="0">Пользователь</option>
                            <?php else:?>
                                <option value="0">Пользователь</option>
                                <option value="1">Администратор</option>
                            <?php endif;?>
                        </select>
                        <?php else:?>
                            <select class="admin-users-status" name="curStatus_<?php echo $user['id']?>">
                                <?php if ($user['status']):?>
                                    <option value="1">Администратор</option>
                                <?php else:?>
                                    <option value="0">Пользователь</option>
                                <?php endif;?>
                            </select>
                        <?php endif;?>
                    </div>
                <?php endforeach; ?>
            </div>
            <input type="submit" name="changeUsers" value="Сохранить" class="btn btn-users-admin">
        </form>
    </div>
</div>