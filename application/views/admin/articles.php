<div class="user-left">
    <div class="admin-logo"></div>
    <a href="/admin/"><button class="user-menu-btn btn">Пользователи</button></a>
    <a href="/admin/orders"><button class="user-menu-btn btn">Заказы</button></a>
    <a href="/admin/articles"><button class="user-menu-btn btn btn-active">Товары</button></a>
</div>

<style>
    .error {
        border-color: red;
    }
</style>

<div class="user-right">
    <div class="user-right-content">
        <span class="admin-articles-span">Добавление нового товара</span>
        <form method="post" enctype="multipart/form-data">
            <input name="name" type="text" class="<?php echo (isset($data['error']['name']) ? 'error' : '') ?> admin-articles-input" value="<?php echo (!empty($data['error']) ? $_POST['name'] : '') ?>" placeholder="Введите название товара">
            <textarea name="desc" id="" cols="30" rows="5" class="<?php echo isset($data['error']['desc']) ? 'error' : '' ?> admin-articles-textarea" placeholder="Введите описание"><?php echo (!empty($data['error']) ? $_POST['desc'] : '') ?></textarea>

            <input name="price" type="number" class="<?php echo isset($data['error']['price']) ? 'error' : '' ?> admin-articles-input"  value="<?php echo (!empty($data['error']) ? $_POST['price'] : '') ?>"placeholder="Введите цену">

            <div class="admin-refactor-sn">
                <select class="<?php echo isset($data['error']['category']) ? 'error' : '' ?> admin-refactor-status" name="category">
                    <?php foreach($data['categories'] as $category):?>
                        <option value="<?php echo $category['id'] ?>" <?php echo (!empty($data['error']) ? $category['id'] == $_POST['category'] ? 'selected' : '' : '')?>><?php echo $category['name']?></option>
                    <?php endforeach;?>
                </select>
                <div class="admin-refactor-number-div">
                    <button class="minus btn minus-plus">-</button>
                    <input type="number" name="count" min="0" max="+50" step="any" value="<?php echo (!empty($data['error']) ? $_POST['count'] : '1') ?>" class="<?php echo isset($data['error']['count']) ? 'error' : '' ?> user-refactor-number">
                    <button class="plus btn minus-plus">+</button>
                </div>

            </div>
            <div  class="admin-articles-file">
                <label class="admin-articles-file-label">
                    <input type="file" name="photo_1" class="admin-articles-file-input" id="input_file_admin" onchange="change();">
                    <span class="admin-articles-file-span" id="text_file_admin">Выберите файл</span>
                </label>
            </div>

            <div  class="admin-articles-file">
                <label class="admin-articles-file-label">
                    <input type="file" name="photo_2" class="admin-articles-file-input">
                    <span  class="admin-articles-file-span">Выберите файл</span>
                </label>
            </div>

            <div  class="admin-articles-file">
                <label class="admin-articles-file-label">
                    <input type="file" name="photo_3" class="admin-articles-file-input">
                    <span  class="admin-articles-file-span">Выберите файл</span>
                </label>
            </div>
            <input type="submit" name="user-login" class="btn user-info-btn" value="Сохранить">

        </form>

    </div>
</div>