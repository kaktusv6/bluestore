<div class="user-left">
    <div class="admin-logo"></div>
    <a href="/admin/"><button class="user-menu-btn btn">Пользователи</button></a>
    <a href="/admin/orders"><button class="user-menu-btn btn btn-active">Заказы</button></a>
    <a href="/admin/articles"><button class="user-menu-btn btn">Товары</button></a>
</div>

<div class="user-right">
    <div class="user-right-content">
<!--            <h2 class="user-info-h2">Список заказов</h2>-->
        <div class="admin-orders-filter">
            <form method="post">
                <span class="admin-orders-filter-span"> Заказы с даты</span>
                <input type="date" class="admin-orders-filter-date">
                <span class="admin-orders-filter-span"> Заказы со статусом</span>
                <select class="admin-order-status">
                    <option>
                        <!--                        Здесь должен отражаться текущий статус заказа-->
                        Статус заказа
                    </option>
                    <option value="1">принят</option>
                    <option value="2">отправлен</option>
                    <option value="3">доставлен</option>
                    <option value="4">выполнен</option>
                </select>
                <input type="submit" value="Применить" class="btn btn-order-admin">
            </form>
        </div>

<div class="user-order">
<!--        /* повторяющаяся часть */-->
        <?php for ($i=0; $i<15; $i++){ ?>
        <a href="/admin/order_details">
            <button class="user-order-btn" >
                <span class="user-order-span"> 000 </span>
                <span class="user-order-span"> 01.11.2002 </span>
                <span class="user-order-span"> выполнено </span>
            </button>
        </a>
<!--        /* конец повторяющейся части */-->
        <?php } ?>
</div>
    </div>
</div>