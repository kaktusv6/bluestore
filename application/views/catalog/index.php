<div class="gap"></div>

<!--<div class="filter">-->
<!--    <div class="filter-add">фильтр +</div>-->
<!--</div>-->

<div class="gap"></div>
<div class="catalog-article">


<!--    --><?php //pp($data)?>
    <?php foreach ($data['items'] as $item): ?>
        <?php
        $isFavorite = false;
        foreach($data['favorites'] as $favorite) {
            if ($favorite['id'] == $item['id']) {
                $isFavorite = true;
                break;
            }
        }
        ?>
        <div class="mini-article">

            <div class="mini-article-logo">
                <button class="mini-article-button-info" onclick='dialogOnClick("/catalog/article_preview/?id=<?php echo $item['id']?>");'>Предпросмотр</button>
                <a href="/catalog/article/<?php echo $item['id']?>"><img src="<?php echo $item['path_image']?>" class="mini-article-logo-img"></a>
            </div>
            <div class="mini-article-info">
                <?php if ($isFavorite):?>
                    <button class="mini-article-favorites mini-article-favorites-active" onclick="dislike(<?php echo $item['id']?>)"></button>
                <?php else:?>
                    <button class="mini-article-favorites" onclick="like(<?php echo $item['id']?>)"></button>
                <?php endif;?>
                <div class="mini-article-price-div"><span class="mini-article-price"><?php echo $item['price']?> руб.</span></div>
                <button class="mini-article-basket" onclick="add_basket(<?php echo $item['id']?>)"></button>
            </div>
        </div>
    <?php endforeach; ?>

    <div class="clear"></div>
<!--    <button class="btn mini-article-button">Показать еще</button>-->
<!--    <div class="catalog-page-info"><span>1 из 1</span></div>-->
</div>
