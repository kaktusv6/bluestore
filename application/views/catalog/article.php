<?php //pp($data)?>
<?php $element = $data['item']; ?>
<span class="article-name"> <?php echo $element['name']?> </span>

<div class="gap"></div>

<div class="article-header">

    <!-- начало слайдера -->
    <div class="article-header-preview" >

        <ul id="lightSlider">
            <li data-thumb="<?php echo $element['path_image']?>" data-src="<?php echo $element['path_image']?>">
                <div class="article-slider-img" style="background-image: url(<?php echo $element['path_image']?>);"></div>
            </li>
            <li data-thumb="<?php echo $element['path_image_2']?>" data-src="<?php echo $element['path_image_2']?>">
                <div class="article-slider-img" style="background-image: url(<?php echo $element['path_image_2']?>); "></div>
            </li>
            <li data-thumb="<?php echo $element['path_image_3']?>" data-src="<?php echo $element['path_image_3']?>">
                <div class="article-slider-img" style="background-image: url(<?php echo $element['path_image_3']?>);"></div>
            </li>
        </ul>
    </div>
    <!-- конец слайдера -->
    <div class="article-header-info">
        <span class="article-price"> <?php echo $element['price']?> руб.</span>

        <div class="article-header-action">
            <button class="article-header-fb" onclick="like(<?php echo $element['id']?>)">
                <div class="article-favorites"></div>
                <span class="article-header-fb-name">
                    В избранное
                </span>
            </button>

            <button class="article-header-fb" onclick="add_basket(<?php echo $element['id']?>)">
                <div class="article-basket"></div>
                <span class="article-header-fb-name">
                    В корзину
                </span>
            </button>
<!--            /*видно только администраторам */-->
            <?php if($data['isAdmin']):?>
            <a href="/admin/article/<?php echo $element['id']?>/" class="article-header-fb-a">
<!--                <button >-->
                    <div class="article-refactor"></div>
                    <span class="article-header-fb-name-a">
                        Изменить
                    </span>
<!--                </button>-->
            </a>
            <?php endif;?>
<!--            /* конец видимой части */-->

        </div>
        <div class="article-content-header">
            <!--        /* здесь впихнуть описание */-->
            <span class="article-content-header-span">
                <?php echo $element['description']?>
            </span>

        </div>
    </div>

</div>
<div class="clear"></div>
<div class="gap"></div>

<div class="article-content">
    <?php if($data['isAuth']):?>
    <div class="article-content-comment-add">
        <form method="post">
            <textarea name="text" id="" cols="30" rows="5" class="article-content-comment-add-textarea" placeholder="Введите комментарий"></textarea>
            <input type="submit" name="send-msg" class="btn article-content-comment-add-btn" value="Отправить">
        </form>
    </div>
    <?php endif;?>

    <?php foreach ($data['comments'] as $comment){ ?>
        <div class="article-content-comment">
            <a href="/admin/user" style="text-decoration: none">
                <span class="article-content-comment-name"><?php echo $comment['name']?></span>
            </a>
            <div class="article-content-comment-m">
                <p><?php echo $comment['messege']?></p>
            </div>
        </div>
    <?php } ?>
<!--    <button class="btn mini-article-button">Показать еще</button>-->
</div>