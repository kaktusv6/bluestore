<?php ob_clean();?>
<?php
$item = $data['item'][0];
?>
<div class="article-preview">
    <span class="article-preview-name"> <?php echo $item['name']?> </span>
    <!-- начало слайдера -->
    <div class="article-preview-slider" >
        <ul id="lightSlider">
            <li data-thumb="<?php echo $item['path_image']?>" data-src="<?php echo $item['path_image']?>">
                <div class="article-preview-slider-img" style="background-image: url(<?php echo $item['path_image']?>);"></div>
            </li>
            <li data-thumb="<?php echo $item['path_image_2']?>" data-src="<?php echo $item['path_image_2']?>">
                <div class="article-preview-slider-img" style="background-image: url(<?php echo $item['path_image_2']?>); "></div>
            </li>
            <li data-thumb="<?php echo $item['path_image_3']?>" data-src="<?php echo $item['path_image_3']?>">
                <div class="article-preview-slider-img" style="background-image: url(<?php echo $item['path_image_3']?>);"></div>
            </li>
        </ul>
    </div>
    <!-- конец слайдера -->

    <div class="article-preview-info">
        <span class="article-preview-price"> <?php echo $item['price']?> руб.</span>

        <div class="article-preview-body">
                <!--        /* здесь впихнуть описание */-->
                <?php echo $item['description']?>
        </div>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#lightSlider").lightSlider({
            item:1,
            gallery: true,
            thumbItem:3,
            thumbMargin: 3,
            // verticalHeight: 300,
            // vertical: true,
            onSliderLoad: function(el) {
                el.lightGallery();
            }
        });
    });
</script>
<?php die();?>

